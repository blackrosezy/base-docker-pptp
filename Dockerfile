FROM ubuntu:trusty

MAINTAINER Mohd Rozi <blackrosezy@gmail.com>

# Install packages
RUN echo "deb http://ppa.launchpad.net/nginx/stable/ubuntu $(lsb_release -cs) main" > /etc/apt/sources.list.d/nginx-stable-$(lsb_release -cs).list

RUN apt-key adv --keyserver keyserver.ubuntu.com --recv-keys C300EE8C

RUN DEBIAN_FRONTEND=noninteractive; apt-get update && apt-get -y \
    install python-pip pptp-linux nginx dos2unix wget curl build-essential \
    && rm -rf /var/lib/apt/lists/*

RUN pip install supervisor dumb-init
RUN mkdir -p /var/log/supervisor

# dcron, a lightweight cron daemon and can access environment variables
RUN curl -L https://www.dropbox.com/s/e05fuqaby0x8hou/dcron.tar.gz?dl=1 -o /tmp/dcron.tar.gz
RUN tar xzvf /tmp/dcron.tar.gz -C /tmp && cd /tmp/dcron-* && make CRONTAB_GROUP=root && make install